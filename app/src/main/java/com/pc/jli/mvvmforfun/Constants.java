package com.pc.jli.mvvmforfun;

public class Constants {

    public static final int CONSTANT_ZERO = 0;
    public static final int CONSTANT_ONE = 1;
    public static final int COLUMNS = 3;

    public static final int REQUEST_RANGE_MIN = CONSTANT_ONE;
    public static final int REQUEST_RANGE_MAX = 100;
    public static final int DEFAULT_TIMEOUT = 8;
    public static final int DEFAULT_START = 0;
    public static final int DEFAULT_RANKING = 10;

    public static final String BINDER_ADAPTER_IMAGE_URL = "imageUrl";
    public static final String BINDER_ADAPTER_RES_ID = "resId";

    public static final String URL_BASE = "https://api.douban.com/v2/movie/";
    public static final String URL_PATH = "top250";
    public static final String URL_PARAM_START = "start";
    public static final String URL_PARAM_COUNT = "count";
}
