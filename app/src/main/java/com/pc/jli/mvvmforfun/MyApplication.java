package com.pc.jli.mvvmforfun;

import android.app.Application;
import android.content.Context;

public class MyApplication extends Application {

    //For Context in Application Class
    //No need to worry about memory leak warning
    private static Context mContext;

    @Override
    public void onCreate() {
        super.onCreate();
        mContext = getApplicationContext();
    }

    //Provide global access for application context
    public static Context getContext() {
        return mContext;
    }
}
