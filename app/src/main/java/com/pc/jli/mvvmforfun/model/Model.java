package com.pc.jli.mvvmforfun.model;

import com.pc.jli.mvvmforfun.model.network.HttpHelper;
import com.pc.jli.mvvmforfun.model.network.bean.GetMovieResponse;
import com.pc.jli.mvvmforfun.model.network.bean.MovieBean;
import com.pc.jli.mvvmforfun.viewmodel.MainViewModel;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.observers.DisposableObserver;
import io.reactivex.schedulers.Schedulers;

import static com.pc.jli.mvvmforfun.Constants.CONSTANT_ZERO;

public class Model {

    private List<MovieBean> mMovieBeanList = new ArrayList<>();

    private Model() {
    }

    //Use private static nested class for thread safe singleton
    public static Model getInstance() {
        return ModelSingletonHolder.mInstance;
    }

    private static class ModelSingletonHolder {
        private static final Model mInstance = new Model();
    }

    public void loadMovieData(int count, final MainViewModel.RequestListener listener) {
        //Use cached data if available
        if (count <= mMovieBeanList.size()) {
            List<MovieBean> list = new ArrayList<>(mMovieBeanList.subList(CONSTANT_ZERO, count));
            if (listener != null) {
                listener.onSuccess(list);
            }
        } else {
            HttpHelper.getMovie(count)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new DisposableObserver<GetMovieResponse>() {
                    @Override
                    public void onNext(GetMovieResponse movieModel) {
                        List<GetMovieResponse.Subject> movieList = movieModel.getSubjects();
                        mMovieBeanList.clear();
                        if (movieList != null && !movieList.isEmpty()) {
                            for (GetMovieResponse.Subject subject : movieList) {
                                String thumbnail = subject.getImages().getMedium();
                                String name = subject.getTitle();

                                MovieBean movieBean = new MovieBean();
                                movieBean.setThumbnail(thumbnail);
                                movieBean.setName(name);
                                mMovieBeanList.add(movieBean);
                            }
                        }
                    }

                    @Override
                    public void onError(Throwable e) {
                        if (listener != null) {
                            listener.onError();
                        }
                    }

                    @Override
                    public void onComplete() {
                        if (listener != null) {
                            listener.onSuccess(mMovieBeanList);
                        }
                    }
                });
        }
    }
}