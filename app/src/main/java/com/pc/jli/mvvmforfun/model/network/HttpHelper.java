package com.pc.jli.mvvmforfun.model.network;

import com.pc.jli.mvvmforfun.Constants;
import com.pc.jli.mvvmforfun.model.network.bean.GetMovieResponse;

import java.util.concurrent.TimeUnit;

import io.reactivex.Observable;
import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

public class HttpHelper {

    public static Observable<GetMovieResponse> getMovie(int count) {
        OkHttpClient httpClient = new OkHttpClient.Builder()
            .connectTimeout(Constants.DEFAULT_TIMEOUT, TimeUnit.SECONDS)
            .readTimeout(Constants.DEFAULT_TIMEOUT, TimeUnit.SECONDS)
            .build();

        Retrofit retrofit = new Retrofit.Builder()
            .baseUrl(Constants.URL_BASE)
            .client(httpClient)
            .addConverterFactory(GsonConverterFactory.create())
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .build();

        NetworkInterface networkInterface = retrofit.create(NetworkInterface.class);
        return networkInterface.getTopMovieRX(Constants.DEFAULT_START, count);
    }
}
