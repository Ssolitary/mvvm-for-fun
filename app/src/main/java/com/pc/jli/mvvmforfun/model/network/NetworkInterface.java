package com.pc.jli.mvvmforfun.model.network;

import com.pc.jli.mvvmforfun.model.network.bean.GetMovieResponse;

import io.reactivex.Observable;
import retrofit2.http.GET;
import retrofit2.http.Query;

import static com.pc.jli.mvvmforfun.Constants.URL_PARAM_COUNT;
import static com.pc.jli.mvvmforfun.Constants.URL_PARAM_START;
import static com.pc.jli.mvvmforfun.Constants.URL_PATH;

public interface NetworkInterface {

    @GET(URL_PATH)
    Observable<GetMovieResponse> getTopMovieRX(@Query(URL_PARAM_START) int start, @Query(URL_PARAM_COUNT) int count);
}