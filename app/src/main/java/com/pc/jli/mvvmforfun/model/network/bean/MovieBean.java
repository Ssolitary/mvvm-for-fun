package com.pc.jli.mvvmforfun.model.network.bean;

import android.databinding.BaseObservable;
import android.databinding.Bindable;

import com.pc.jli.mvvmforfun.BR;

public class MovieBean extends BaseObservable {

    //LiveData is currently not supported inside bean
    //Use DataBinding instead
    private String mThumbnail;
    private String mName;

    @Bindable
    public String getThumbnail() {
        return mThumbnail;
    }

    public void setThumbnail(String thumbnail) {
        mThumbnail = thumbnail;
        notifyPropertyChanged(BR.thumbnail);
    }

    @Bindable
    public String getName() {
        return mName;
    }

    public void setName(String name) {
        mName = name;
        notifyPropertyChanged(BR.name);
    }
}
