package com.pc.jli.mvvmforfun.view;

import android.arch.lifecycle.ViewModelProvider;
import android.arch.lifecycle.ViewModelProviders;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.TextView;
import android.widget.Toast;

import com.pc.jli.mvvmforfun.Constants;
import com.pc.jli.mvvmforfun.R;
import com.pc.jli.mvvmforfun.databinding.ActivityMainBinding;
import com.pc.jli.mvvmforfun.model.network.bean.MovieBean;
import com.pc.jli.mvvmforfun.view.adapter.MovieAdapter;
import com.pc.jli.mvvmforfun.view.helpers.DialogHelper;
import com.pc.jli.mvvmforfun.view.helpers.StringHelper;
import com.pc.jli.mvvmforfun.view.helpers.UIHelper;
import com.pc.jli.mvvmforfun.viewmodel.MainViewModel;
import com.pc.jli.mvvmforfun.viewmodel.UIEvent;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.List;

public class MainActivity extends AppCompatActivity {

    private ActivityMainBinding mActivityMainBinding;
    private MainViewModel mMainViewModel;
    private MovieAdapter mMovieAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        EventBus.getDefault().register(this);
        setContentView(R.layout.activity_main);
        initDataBinding();
        setupView();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        EventBus.getDefault().unregister(this);
    }

    private void initDataBinding() {
        mActivityMainBinding = DataBindingUtil.setContentView(this, R.layout.activity_main);
        mMainViewModel = ViewModelProviders.of(this, new ViewModelProvider.NewInstanceFactory()).get(MainViewModel.class);
        mActivityMainBinding.setViewModel(mMainViewModel);
        mActivityMainBinding.setLifecycleOwner(this);
    }

    private void setupView() {
        //Change done button behavior for soft keyboard
        mActivityMainBinding.searchEditText.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_DONE) {
                    mMainViewModel.queryMovies();
                    return true;
                }
                return false;
            }
        });

        initRecyclerView();
    }

    private void initRecyclerView() {
        GridLayoutManager layoutManager = new GridLayoutManager(this, Constants.COLUMNS);
        mActivityMainBinding.movieRv.setLayoutManager(layoutManager);
        mMovieAdapter = new MovieAdapter(mMainViewModel);
        mActivityMainBinding.movieRv.setAdapter(mMovieAdapter);
    }

    //Handle one-time UI event
    private void handleUIEvent(UIEvent uiEvent) {
        switch (uiEvent.getType()) {
            case SHOW_TOAST:
                String msg = uiEvent.getStringValue();
                if (!TextUtils.isEmpty(msg)) {
                    Toast.makeText(this, msg, Toast.LENGTH_SHORT).show();
                }
                break;
            case SHOW_DIALOG:
                if(uiEvent.getBooleanValue()){
                    DialogHelper.getInstance().show(MainActivity.this, StringHelper.getString(R.string.loading_msg));
                } else {
                    DialogHelper.getInstance().close();
                }
                break;
            case CLEAR_FOCUS:
                View view = getCurrentFocus();
                if (view != null) {
                    view.clearFocus();
                }
                break;
            case HIDE_KEYBOARD:
                UIHelper.hideSoftKeyboard(this);
                break;
        }
    }

    //Handle Eventbus events
    @Subscribe(threadMode = ThreadMode.MAIN)
    public void receiveMovieData(List<MovieBean> list) {
        mMovieAdapter.refreshData(list);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void receiveUIEvent(UIEvent uiEvent) {
        handleUIEvent(uiEvent);
    }
}