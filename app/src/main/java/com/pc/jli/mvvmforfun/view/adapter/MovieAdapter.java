package com.pc.jli.mvvmforfun.view.adapter;

import android.databinding.DataBindingUtil;
import android.databinding.ViewDataBinding;
import android.view.ViewGroup;

import com.pc.jli.mvvmforfun.BR;
import com.pc.jli.mvvmforfun.R;
import com.pc.jli.mvvmforfun.model.network.bean.MovieBean;
import com.pc.jli.mvvmforfun.view.helpers.StringHelper;
import com.pc.jli.mvvmforfun.viewmodel.MainViewModel;

import java.util.Locale;

public class MovieAdapter extends BaseAdapter<MovieBean, BaseViewHolder> {

    private final MainViewModel mMainViewModel;

    public MovieAdapter(MainViewModel mainViewModel) {
        super();
        mMainViewModel = mainViewModel;
    }

    @Override
    public BaseViewHolder onCreateVH(ViewGroup parent, int viewType) {
        ViewDataBinding dataBinding = DataBindingUtil.inflate(mInflater, R.layout.movie_holder, parent, false);
        return new BaseViewHolder(dataBinding);
    }

    //Passing data into view holder
    @Override
    public void onBindVH(BaseViewHolder baseViewHolder, int position) {
        ViewDataBinding binding = baseViewHolder.getBinding();
        binding.setVariable(BR.movieBean, mList.get(position));
        binding.setVariable(BR.adapter, this);
        binding.setVariable(BR.position, position);
        binding.executePendingBindings();
    }

    //Demo for retrieving data from view holder
    public void clickMovie(MovieBean movieBean, int position) {
        String format = StringHelper.getString(R.string.clicked_msg);
        mMainViewModel.showToast(String.format(Locale.US, format, movieBean.getName(), position + 1));
    }
}