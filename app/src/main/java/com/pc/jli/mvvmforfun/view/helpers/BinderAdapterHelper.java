package com.pc.jli.mvvmforfun.view.helpers;

import android.databinding.BindingAdapter;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.pc.jli.mvvmforfun.Constants;

public class BinderAdapterHelper {

    @BindingAdapter({Constants.BINDER_ADAPTER_IMAGE_URL})
    public static void loadImage(ImageView iv, String url) {
        Glide.with(iv.getContext()).load(url).into(iv);
    }

    @BindingAdapter({Constants.BINDER_ADAPTER_RES_ID})
    public static void loadMipmapResource(ImageView iv, int resId) {
        iv.setImageResource(resId);
    }
}
