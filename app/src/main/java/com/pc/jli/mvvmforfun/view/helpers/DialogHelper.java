package com.pc.jli.mvvmforfun.view.helpers;

import android.app.ProgressDialog;
import android.content.Context;

public class DialogHelper {

    private ProgressDialog mProgressDialog;

    private DialogHelper() {
    }

    //Use private static nested class for thread safe singleton
    public static DialogHelper getInstance() {
        return DialogHelperSingletonHolder.mInstance;
    }

    private static class DialogHelperSingletonHolder {
        private static final DialogHelper mInstance = new DialogHelper();
    }

    public void show(Context context, String msg) {
        close();
        if (mProgressDialog == null) {
            createDialog(context);
        }
        mProgressDialog.setMessage(msg);
        mProgressDialog.show();
    }

    public void close() {
        if (mProgressDialog != null && mProgressDialog.isShowing()) {
            mProgressDialog.dismiss();
        }
    }

    private void createDialog(Context context) {
        mProgressDialog = new ProgressDialog(context);
        mProgressDialog.setCancelable(false);
        mProgressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
    }
}
