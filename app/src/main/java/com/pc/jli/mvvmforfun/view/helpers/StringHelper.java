package com.pc.jli.mvvmforfun.view.helpers;

import android.support.annotation.PluralsRes;
import android.support.annotation.StringRes;

import com.pc.jli.mvvmforfun.MyApplication;

public class StringHelper {

    public static String getString(@StringRes int resId) {
        return MyApplication.getContext().getString(resId);
    }

    public static String getQuantityStrings(@PluralsRes int id, int quantity, Object... formatArgs) {
        return MyApplication.getContext().getResources().getQuantityString(id, quantity, formatArgs);
    }
}
