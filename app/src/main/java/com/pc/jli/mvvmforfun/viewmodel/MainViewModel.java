package com.pc.jli.mvvmforfun.viewmodel;

import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.ViewModel;
import android.text.TextUtils;

import com.pc.jli.mvvmforfun.Constants;
import com.pc.jli.mvvmforfun.R;
import com.pc.jli.mvvmforfun.model.Model;
import com.pc.jli.mvvmforfun.model.network.bean.MovieBean;
import com.pc.jli.mvvmforfun.view.helpers.StringHelper;

import org.greenrobot.eventbus.EventBus;

import java.util.List;
import java.util.Locale;

public class MainViewModel extends ViewModel {

    // LiveData variables
    private final MutableLiveData<String> mRanking = new MutableLiveData<>();
    private final MutableLiveData<String> mTitle = new MutableLiveData<>();

    public MainViewModel() {
        mRanking.setValue(String.valueOf(Constants.DEFAULT_RANKING));
        queryMovies();
    }

    public void queryMovies() {
        if (hasError()) {
            return;
        }
        getNewsData();
        hideKeyboard();
    }

    //Check input range
    private boolean hasError() {
        String input = mRanking.getValue();
        if (TextUtils.isEmpty(input)) {
            showToast(StringHelper.getString(R.string.empty_input_msg));
            return true;
        } else {
            int intVal = Integer.valueOf(input);
            if (intVal < Constants.REQUEST_RANGE_MIN || intVal > Constants.REQUEST_RANGE_MAX) {
                String format = StringHelper.getString(R.string.error_input_msg);
                showToast(String.format(Locale.US, format, Constants.REQUEST_RANGE_MIN, Constants.REQUEST_RANGE_MAX));
                return true;
            } else {
                return false;
            }
        }
    }

    private void getNewsData() {
        showDialog(true);
        Model.getInstance().loadMovieData(Integer.valueOf(mRanking.getValue()), new RequestListener() {
            @Override
            public void onSuccess(List<MovieBean> movieBeanList) {
                EventBus.getDefault().post(movieBeanList);
                updateTitle(movieBeanList.size());
                showDialog(false);
            }

            @Override
            public void onError() {
                showToast(StringHelper.getString(R.string.request_failed));
                showDialog(false);
            }
        });
    }

    private void updateTitle(int count) {
        mTitle.setValue(StringHelper.getQuantityStrings(R.plurals.movie_title, count, count));
    }

    public void showToast(String message) {
        UIEvent event = new UIEvent(UIEvent.eEventType.SHOW_TOAST, message);
        EventBus.getDefault().post(event);
    }

    private void showDialog(boolean isShow) {
        UIEvent event = new UIEvent(UIEvent.eEventType.SHOW_DIALOG, isShow);
        EventBus.getDefault().post(event);
    }

    private void hideKeyboard() {
        UIEvent event = new UIEvent(UIEvent.eEventType.HIDE_KEYBOARD);
        EventBus.getDefault().post(event);
        clearFocus();
    }

    private void clearFocus() {
        UIEvent event = new UIEvent(UIEvent.eEventType.CLEAR_FOCUS);
        EventBus.getDefault().post(event);
    }

    public MutableLiveData<String> getRanking() {
        return mRanking;
    }

    public MutableLiveData<String> getTitle() {
        return mTitle;
    }

    public interface RequestListener {
        void onSuccess(List<MovieBean> movieBeanList);

        void onError();
    }
}