package com.pc.jli.mvvmforfun.viewmodel;

public class UIEvent {

    private final eEventType mType;
    private String mStringValue;
    private Boolean mBooleanValue;

    public enum eEventType {
        SHOW_TOAST,
        SHOW_DIALOG,
        CLEAR_FOCUS,
        HIDE_KEYBOARD
    }

    public UIEvent(eEventType type) {
        this.mType = type;
    }

    public UIEvent(eEventType type, String stringValue) {
        this.mType = type;
        this.mStringValue = stringValue;
    }

    public UIEvent(eEventType type, boolean booleanValue) {
        this.mType = type;
        this.mBooleanValue = booleanValue;
    }

    public eEventType getType() {
        return mType;
    }

    public String getStringValue() {
        return mStringValue;
    }

    public boolean getBooleanValue() {
        return mBooleanValue;
    }
}